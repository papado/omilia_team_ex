import React from "react";
import {
    GeneralStyle,
    GoHomeButton,
    PageNotFound,
    PageTitle,
    TopBarContainer,

} from "../generalStyle";
import {useHistory} from "react-router";

const ErrorPage = () => {
    let history = useHistory()
    return (
        <GeneralStyle>
            <TopBarContainer>
                <PageTitle> </PageTitle>
                <GoHomeButton onClick={() => history.push('/')}>Home</GoHomeButton>

            </TopBarContainer>
            <div>

                <PageNotFound>
                    <div className='hugeText'>404</div>
                    Sorry! Page Not Found!
                </PageNotFound>
            </div>


        </GeneralStyle>

    )
}

export default ErrorPage