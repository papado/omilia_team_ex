import React, {useEffect, useState} from "react";
import {deleteUser, fetchUsers} from "../../helpers/backendCalls";
import UserViewCard from "../user/userViewCard";
import {
    AddButton, Dimmer,
    GeneralStyle,
    JustifyCenter,
    ListItemDiv,
    PageTitle,
    TopBarContainer,
    UsersList
} from "../../generalStyle";
import {useHistory} from "react-router";

const HomeIndex = () => {
    let history = useHistory()
    const [users, setUsers] = useState()
    const [showDimmer, setShowDimmer] = useState(false)


    useEffect(() => {
        setShowDimmer(true)
        fetchUsers()
            .then(rest => {
                setUsers(rest)
                setShowDimmer(false)
            })
            .catch(e => {
                setShowDimmer(false)
            })
    }, [])

    const onDelete = (id) => {
        setShowDimmer(true)
        deleteUser(id)
            .then(rest => {
                setUsers(rest)
                setShowDimmer(false)
            })
            .catch(e => {
                setShowDimmer(false)
            })
    }


    return (

        <GeneralStyle>
            <TopBarContainer>
                <PageTitle>Home Page</PageTitle>
                {users && users.data.length &&
                <AddButton onClick={() => history.push('/users/create')}>Add User</AddButton>}

            </TopBarContainer>
            {showDimmer &&
            <Dimmer>
                <JustifyCenter>
                    <div className='dimmerLabel'>Loading...</div>
                </JustifyCenter>
            </Dimmer>}

            {users && users.data.length ?
                <UsersList>
                    {users.data.map(user => <UserViewCard onDelete={() => onDelete(user._id)} user={user}/>).reverse()}
                </UsersList>
                :
                <JustifyCenter>
                    <ListItemDiv>
                        <div className='justifyText'>List of users is empty please insert your first user.</div>
                        <AddButton onClick={() => history.push('/users/create')}>Add User</AddButton>
                    </ListItemDiv>
                </JustifyCenter>
            }
        </GeneralStyle>

    )
}

export default HomeIndex