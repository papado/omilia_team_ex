import React, {useEffect, useState} from "react";
import {
    DeleteButton, Dimmer, FormContainer, FormDiv,
    GeneralStyle,
    GoHomeButton, Input, InputDiv, JustifyCenter,  NotificationError,  PageTitle,
    SaveButton,
    TopBarContainer,
    TwoButtonContainer
} from "../../generalStyle";
import {useHistory} from "react-router";
import {addUser, deleteUser, editUser} from "../../helpers/backendCalls";
import {isValidForm, ValidMail, ValueIsEmpty} from "../../helpers/validator";


const UserNewEditCard = () => {
    const [userData, setUserData] = useState({})
    const [showNotification, setShowNotification]  = useState(false)
    const [showDimmer, setShowDimmer] = useState(false)
    const [fieldsWithError, setFieldsWithError] = useState({})
    const [errorMsg, setErrorMsg] = useState()
    let history = useHistory()
    let editMode = history.location.state && history.location.state.user
    let title =  editMode ? 'Edit User' : 'Add User'


    useEffect(()=>{
         history.location.state && history.location.state.user && setUserData(history.location.state.user)
    },[])


    const resetNotification = () => {
        setTimeout(()=>{
            setShowNotification(false)
        },2000)
        return null
    }

    const showNotificationMsg = (msg) =>{
        setErrorMsg(msg);
        setShowNotification(true)
        resetNotification()
    }


    const onChangeInput = (event) => {
        let {name ,value} = event.target
        setUserData({...userData, [name]: value})
    }

    const onSave = async () =>{
        setShowDimmer(true)
        if(isValidForm(fieldsWithError) ) {
            editMode ?
                editUser(userData)
                    .then(rest => history.push('/'))
                    .catch(e => {
                        setShowDimmer(false)
                        showNotificationMsg(`Please check again all fields. ${e}`)
                    })
                :
                addUser(userData)
                    .then(rest => history.push('/'))
                    .catch(e => {
                        setShowDimmer(false)
                        showNotificationMsg(`Please check again all fields. ${e}`)
                    })

        }else {
            setShowDimmer(false)
            setTimeout(()=>{
                showNotificationMsg(`Please check again all fields.`)
            },500)

        }

    }

    const onDelete = () => {
        setShowDimmer(true)
        deleteUser(userData._id)
            .then(rest=>history.push('/'))
            .catch(e=>{
                setShowDimmer(false)
                showNotificationMsg(`Something go wrong.${e}Please try again.`)
            })
    }

    const fieldIsValid = (event) => {
        let {name} = event.target

        let fieldName = {
            firstName: 'First Name',
            lastName: 'Last Name',
            email: 'E-mail'
        }[name]

        userData[name] && !ValueIsEmpty(userData[name])
            ? isValid(name)
            : isInvalid(name, `${fieldName} is required!!!`)

        name == 'email' && userData[name] &&  !ValueIsEmpty(userData[name]) && !ValidMail(userData[name])
            ? isInvalid(name,'Not valid e-mail format.')
            : isValid(name)

    }

    const isInvalid = (name,msg) => {
        console.log('is invalid')
        setFieldsWithError({...fieldsWithError , [name]: true } )
        showNotificationMsg(msg)
        return null
    }

    const isValid = (name) => {
        console.log('is valid')
        setFieldsWithError({...fieldsWithError, [name]: false} )
        return null
    }


    return(

        <GeneralStyle>
            <TopBarContainer>
                <PageTitle>{title}</PageTitle>

                <GoHomeButton onClick={()=>history.push('/')}>Home</GoHomeButton>
            </TopBarContainer>
            <FormContainer>
                    <FormDiv>
                        <InputDiv>
                            <h2>First Name</h2>
                            <Input
                                type="text"
                                name='firstName'
                                value={userData ? userData.firstName : ''}
                                onChange={onChangeInput}
                                onBlur={fieldIsValid}
                            />
                        </InputDiv>
                        <InputDiv>
                            <h2>Last Name</h2>
                            <Input
                                type="text"
                                name='lastName'
                                value={userData ? userData.lastName: ''}
                                onChange={onChangeInput}
                                onBlur={fieldIsValid}
                            />
                        </InputDiv>
                        <InputDiv>
                            <h2>Email</h2>
                            <Input
                                type="text"
                                name='email'
                                value={userData ? userData.email : ''}
                                onChange={onChangeInput}
                                onBlur={fieldIsValid}

                            />
                        </InputDiv>
                    </FormDiv>

            </FormContainer>

            <TwoButtonContainer>
                <SaveButton onClick={onSave}>Save</SaveButton>
                {editMode && <DeleteButton onClick={onDelete}>Delete</DeleteButton> }
            </TwoButtonContainer>

            {showDimmer &&
            <Dimmer>
                <JustifyCenter>
                    <div className='dimmerLabel'>Loading...</div>
                </JustifyCenter>
            </Dimmer>}

            {showNotification &&
                <NotificationError>
                    {errorMsg}
                </NotificationError>
            }
        </GeneralStyle>

    )









}

export default UserNewEditCard
