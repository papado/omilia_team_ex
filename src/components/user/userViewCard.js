import React from "react";
import {ButtonsHeader, DeleteSmall, EditSmall,ListItemDiv} from "../../generalStyle";
import {useHistory} from "react-router";

const UserViewCard = ({user, onDelete}) => {
    let history = useHistory()

    return(
            <ListItemDiv>
                <span> Name : {user.firstName}</span>
                <span> Last Name : {user.lastName}</span>
                <span> E-mail : {user.email}</span>
                <ButtonsHeader >
                    <EditSmall onClick={ ()=> history.push({pathname:`users/${user._id}`, state:{user}})}>Edit</EditSmall>

                    <DeleteSmall onClick={onDelete}>Delete</DeleteSmall>
                </ButtonsHeader>
            </ListItemDiv>

    )
}

export default UserViewCard