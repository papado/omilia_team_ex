import axios from "axios";

const URL = `/api/`


export const fetchUsers = async () => {

    return new Promise(((resolve, reject) => {
        axios({
            method: "GET",
            url: `${URL}users`,
            headers: {"Content-Type": "application/json"},

        })
            .then(response => resolve(response.data))
            .catch(error => reject(error.response.data.message));
    }))

};


export const editUser = async data => {

    return new Promise(((resolve, reject) => {
        axios({
            method: "PUT",
            url: `${URL}users/${data._id}`,
            headers: {"Content-Type": "application/json"},
            data: {
                firstName: data.firstName,
                lastName: data.lastName,
                email: data.email
            }
        }).then(response => resolve(response.data))
            .catch(error => reject(error.response.data.message))
    }))


};

export const addUser = async data => {

    return new Promise(((resolve, reject) => {
        axios({
            method: "POST",
            url: `${URL}users`,
            headers: {"Content-Type": "application/json"},
            data: {
                firstName: data.firstName,
                lastName: data.lastName,
                email: data.email
            }
        }).then(response => resolve(response.data))
            .catch(error => reject(error.response.data.message));
    }))


};

export const deleteUser = async id => {

    return new Promise(((resolve, reject) => {
        axios({
            method: "delete",
            url: `${URL}users/${id}`,
            headers: {"Content-Type": "application/json"},
        })
            .then(response => resolve(response.data))
            .catch(error => reject(error.response.data.message));
    }))

};
