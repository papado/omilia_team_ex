
export const ValidMail = (mail) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return  re.test(String(mail).toLowerCase())
}

export const ValueIsEmpty = (value) => {
    console.log('!value.trim()' , !value.trim())
    return  !value.trim()
}



export const isValidForm = errors => {
    let isValArray = Object.values(errors).map((current) => current);
    let returnableIsValid = !isValArray.includes(true);
    return returnableIsValid;
}
