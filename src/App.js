import React, {Suspense} from "react";
import {Redirect, Route, Router, Switch} from "react-router";
import {BrowserRouter} from "react-router-dom";
import {Dimmer} from "./generalStyle";

const HomeIndex = React.lazy(() => import('./components/home/homeIndex'))
const ErrorPage = React.lazy(() => import('./components/errorPage'))
const UserNewEditPage = React.lazy(() => import('./components/user/userNewEditCard'))

const App = () => {

    return (
        <Suspense fallback={<Dimmer>Loading...</Dimmer>}>
            <BrowserRouter>
                <Switch>

                    <Route exact path='/users'>
                        <HomeIndex/>
                    </Route>
                    <Redirect from='/' to='/users' exact/>
                    <Route exact path='/users/create'>
                        <UserNewEditPage/>
                    </Route>
                    <Route exact path='/users/:id'>
                        <UserNewEditPage/>
                    </Route>
                    <Route path='/*'>
                        <ErrorPage/>
                    </Route>
                </Switch>
            </BrowserRouter>
        </Suspense>
    )

}

export default App