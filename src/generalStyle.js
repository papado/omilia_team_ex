import styled from "styled-components";

let deleteColor = "#ef5350";
let editColor = "#ffa726";
let addColor = "#689f38";
let saveColor = "#689f38";
let homeColor = "#e0f7fa";
let borderAndText = "#263238"
let backgroundDivs = "#b3e5fc"
let notificationError = "#ffe0b2"


export const Dimmer = styled.div`
  background: #000;
  opacity: 0.7;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 9999;

  .dimmerLabel {
    font-size: 25px;
    color: white;
  }
`

export const NotificationError = styled.div`
  display: flex;
  justify-content: center;
  padding: 15px;
  position: absolute;
  bottom: 0;
  border: 2px solid ${borderAndText};
  border-radius: 25px;
  left: 50%;
  transform: translate(-50%, -50%);
  height: 50px;
  background-color: ${notificationError};

`
export const GeneralStyle = styled.div`
  background-color: ${homeColor};
  min-height: 100vh;
  min-width: 80vw;
  display: flex;
  flex-direction: column;
  font-size: calc(5px + 2vmin);
  color: ${borderAndText};
`;

export const UsersList = styled.div`
  padding: 5%;
  justify-content: center;
  display: flex;
  flex-wrap: wrap;
`;

export const PageNotFound = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  font-size: xxx-large;
  color: ${deleteColor};
  transform: translate(-50%, -50%);

  .hugeText {
    font-size: 300px;
  }
`

export const TopBarContainer = styled.div`
  padding: 15px;
  display: flex;
  justify-content: space-between

`;

export const PageTitle = styled.div`
  align-self: center;
  width: 150px;
  font-size: 30px;
  color: ${borderAndText};
  padding: 15px

`;

export const ListItemContainer = styled.div`
  padding: 0px;
  position: relative;


`;

export const ButtonsHeader = styled.div`

  display: flex;
  justify-content: space-around;
`

export const JustifyCenter = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

`

export const ListItemDiv = styled.div`

  padding: 10px;
  margin: 10px;
  width: 400px;
  @media screen and (max-width: 700px) {
    width: 250px;
  }
  height: 150px;
  flex-direction: column;
  justify-content: space-between;
  border: 2px solid ${borderAndText};
  box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
  border-radius: 25px;
  display: flex;
  background-color: ${backgroundDivs};
  color: ${borderAndText};

  .justifyText {
    padding: 10px;
  }

`;

export const FormDiv = styled.div`
  width: 50%;
  padding-bottom: 20px;
  border: 2px solid ${borderAndText};
  box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
  flex-direction: column;
  border-radius: 25px;
  display: flex;
  background-color: ${backgroundDivs};
  color: black;

`;

export const InputDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`


export const FormContainer = styled.div`
  //transform: translate(15%, 5%);
  display: flex;
  justify-content: center;
  padding-top: 5%;
`;

// BUTTON

export const AddButton = styled.div`

  display: flex;
  justify-content: center;
  min-width: 100px;
  border: 2px solid ${addColor};
  box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
  border-radius: 15px;
    //  background-color: ${addColor};
  color: ${addColor};
  padding: 15px;
  cursor: pointer;
`;

export const TwoButtonContainer = styled.div`
  padding-top: 35px;
  display: flex;
  justify-content: center;
`;

export const SaveButton = styled.div`
  display: flex;
  justify-content: center;
  min-width: 70px;
  border: 2px solid ${saveColor};
  box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
  border-radius: 15px;
    // background-color: ${saveColor};
  color: ${saveColor};
  padding: 15px;
  cursor: pointer

`;
export const Input = styled.input`

  background-color: ${backgroundDivs};
  border: 2px none;
  border-bottom: 2px solid ${borderAndText};
  width: 70%;
  height: 25px;
  font-size: 25px;
  text-align: center

`;

export const DeleteButton = styled.div`
  display: flex;
  justify-content: center;
  min-width: 70px;


  margin-left: 10px;
  border: 2px solid ${deleteColor};
  box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
  border-radius: 15px;
    // background-color: ${deleteColor};
  color: ${deleteColor};
  padding: 15px;
  cursor: pointer

`;
export const GoHomeButton = styled.div`
  display: flex;
  justify-content: center;
  min-width: 100px;
  border: 2px solid ${editColor};
  box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
  border-radius: 15px;
    // background-color: ${editColor};
  color: ${editColor};
  padding: 15px;
  cursor: pointer

`;

export const DeleteSmall = styled.div`
  width: 30px;
  justify-content: center;
  font-weight: bold;

  display: flex;
  height: 15px;
  font-size: 13px;
  border: 2px solid ${deleteColor};
  box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
  border-radius: 15px;
    //  background-color: ${deleteColor};
  color: ${borderAndText};
  padding: 15px;
  cursor: pointer

`;
export const EditSmall = styled.div`
  width: 30px;
  justify-content: center;
  font-weight: bold;
  display: flex;
  height: 15px;
  font-size: 13px;
  border: 2px solid ${editColor};
  box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
  border-radius: 15px;
  color: ${borderAndText};
  padding: 15px;
  cursor: pointer

`;
