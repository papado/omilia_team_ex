import React from 'react';
import ReactDOM from 'react-dom';

import "core-js/stable";
import "regenerator-runtime/runtime";
import App from './App';


const render = () => {
    ReactDOM.render(<App/>, document.getElementById('root'));
};

render();

if (process.env.NODE_ENV !== 'production' && module.hot) {
    module.hot.accept('./App', render);
}
