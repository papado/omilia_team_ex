const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');



module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].bundle.js',
        sourceMapFilename: '[file].js.map',
        publicPath: `/`,
    },
    devServer: {
        proxy:{
            '/api': {
                target : 'https://api-users.fakiolinho.vercel.app/api',
                pathRewrite: { '^/api': '' },
                changeOrigin: true,
                secure: false

            }
        },
        historyApiFallback: true,
        host: '0.0.0.0',
        port: 8089,
    },
    resolve: {
        modules: [path.resolve(__dirname, 'src'), 'node_modules'],
        extensions: ['*', '.js', '.jsx'],
    },

    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    },
                },
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ]
            },

        ],
    },
    plugins: [
        new HtmlWebPackPlugin({ template: path.resolve(__dirname, 'public/index.html') }),
    ],

};