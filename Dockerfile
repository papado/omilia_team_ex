# base image
FROM node:14.15.0-alpine as builder

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh vim

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
RUN npm install --silent

# start app
CMD ["npm","start"]



