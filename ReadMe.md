


## About The Project

This project is a foul responsive Single Page Application exercise with custom style.

Created by Papadopoulos Pavlos for Omilia.



## Deployment

---
To get a local copy up and running follow these simple steps.

 1. Clone the project 
```bash
git clone git@gitlab.com:papado/omilia_team_ex.git
```

 2. Go to project directory in terminal and run a command:

- With Docker

```bash
docker-compose up --build
```

- Without Docker

```bash
npm install
npm start
```

if everything goes to plan, project is running at http://localhost:8089/

## Build with

---

- [React 17.0.2](https://reactjs.org/)
- [Webpack 5.37.0](https://webpack.js.org/)
- [Styled Components 5.3.0](https://www.styled-components.com/)
- [React-router 5.2.0](https://reactrouter.com/) 
- [Axios 0.21.1](https://axios-http.com/)

## Contact

---
Name: Papadopoulos Pavlos 
    
E-mail: papado2007@gmail.com

Project Link: https://gitlab.com/papado/omilia_team_ex.git
